# ZoomConnect API tests update 123

**ZoomConnect app API tests using Java Serenity**
# About

- Testing endpoints for managing contacts on ZoomConnect application using SerenityBDD and Cucumber 6
- The endpoints that were tested :


    **get** _/api/rest/v1/contacts/all_ - getting all contacts

    **get** _/api/rest/v1/contacts/{contactId}_ - getting one contact

    **post** _/api/rest/v1/contacts/create_ - creating new contact

    **post** _/api/rest/v1/contacts/{contactId}_ - updating the contact with provided contactId

    **delete** _/api/rest/v1/contacts/{contactId}_ - deleting the contact with provided contactId

- API documentation can be found on the following link: https://www.zoomconnect.com/interactive-api/

## Structure
- _zoomconnectapp/CucumberRunner.java_ - defining the runner
- _src/main/java/model/_ - class for defining the contacts properties
- _src/test/java/zoomconnectapp/specifications_ - class for providing baseURI, basePath and authentication
- _src/test/java/zoomconnectapp/api_ - class for defining api
- _src/test/java/zoomconnectapp/steps_ - class for defining Given/When/Then steps
- _src/test/resources/features_ - feature files


## Installation
- Install Maven 3.8.1 and Java 11
- Add Serenity BDD dependencies in pom.xml file

## How to run tests
- **JUnit**: run class CucumberRunner.java
- **Maven**: run command from project :```mvn clean verify ``` or to run tests with tags : ```mvn clean verify -Dcucumber.filter.tags=@smoke ```

- Test reports are generated with maven run and can be accessed at _target/site/serenity/index.html_
