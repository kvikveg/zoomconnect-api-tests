Feature: Updating a contact

  @smoke
  Scenario: Updating a contact with a valid contact number
    When user updates the contact number with '+17059850640' of a first contact in the list
    Then contact is available with contact number '+17059850640'
    And the status code should be 200

  Scenario: Updating a contact with invalid contact number
    When user updates the contact number with '17103589127' of a first contact in the list
    Then the status code should be 400
    And the message should be 'Invalid mobile number'
