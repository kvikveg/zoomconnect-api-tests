Feature: Getting contacts

  @smoke
  Scenario: Getting all created contacts
    When user sends a request for getting all contacts
    Then the status code should be 200

  Scenario: User is not able to get all contacts when provides invalid auth token
    When user provides invalid token with getting all contacts
    Then the status code should be 401
    And the message should be 'Reason: Unauthorized'

  Scenario: Getting a contact with valid id
    When user gets the first contact in the list
    Then the status code should be 200
    And the first contact is available

  Scenario: Getting a contact with invalid id
    When user tries to get a contact with invalid id
    Then the status code should be 404