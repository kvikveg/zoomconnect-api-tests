Feature: Creating a contact

  @smoke
  Scenario Outline: Creating a new contacts with valid information
    When user creates new contact with information
      | contactNumber   | firstName   | lastName   | title   |
      | <contactNumber> | <firstName> | <lastName> | <title> |
    Then new contact is available with
      | contactNumber   | firstName   | lastName   | title   |
      | <contactNumber> | <firstName> | <lastName> | <title> |
    And the status code should be 201
    Examples:
      | contactNumber   | firstName | lastName | title         |
      | +17813933677    | Dragana   | Kuzminac | Hello Dragana |
      | +1-615-941-4024 | Marko     | Markovic | Hello Marko   |


  Scenario Outline: Creating a new contact with invalid contact number
    When user creates new contact with information
      | contactNumber   | firstName   | lastName   | title   |
      | <contactNumber> | <firstName> | <lastName> | <title> |
    Then the status code should be 400
    And the message should be 'Invalid mobile number'
    Examples:
      | contactNumber | firstName | lastName  | title        |
      | +11234        | Petar     | Petrovic  | Hello Petar  |
      | 17813933677   | Jovan     | Jovanovic | Hello Jovan  |
      |               | Stanko    | Stanic    | Hello Stanko |

