Feature: Deleting a contact

  @smoke
  Scenario: Deleting a contact providing a valid id
    When user deletes the first contact in the list
    Then the status code should be 200

  Scenario: Deleting a contact providing a invalid id
    When user deletes the contact with invalid contactId
    Then the status code should be 404
