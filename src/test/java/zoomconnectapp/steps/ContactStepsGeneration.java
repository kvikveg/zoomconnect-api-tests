package zoomconnectapp.steps;

import io.cucumber.java.en.And;
import io.restassured.response.Response;
import model.Contact;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import zoomconnectapp.api.ContactApi;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ContactStepsGeneration {

    Response response;
    Contact contact;

    @Steps
    ContactApi contactApi;

    @When("user provides invalid token with getting all contacts")
    public void userProvidesInvalidTokenWithGetRequestTheStatusCodeShouldBe() {

        response = contactApi.getAllContactsWithInvalidToken();
    }

    @When("user sends a request for getting all contacts")
    public void sendsGetRequestForGettingAllUsers() {

        response = contactApi.getAllContacts();
    }

    @When("user creates new contact with information")
    public void userCreatesContactWithInformation(List<Map<String, String>> data){

        response = contactApi.createContact(data);
    }

    @When("user deletes the first contact in the list")
    public void userDeletesTheContactWithContactId() {

        response = contactApi.deleteContact();
    }

    @When("user deletes the contact with invalid contactId")
    public void userDeletesTheContactWithInvalidContactId() {

        response = contactApi.deleteInvalidContact();
    }

    @When("user updates the contact number with {string} of a first contact in the list")
    public void userUpdatesTheContactNumberOfAUserWithIdContactId(String contactNumber) {

        response = contactApi.updateContact(contactNumber);
    }

    @When("user gets the first contact in the list")
    public void userProvidesAContactId() {

        response = contactApi.getContactByContactId();
    }

    @When("user tries to get a contact with invalid id")
    public void userProvidesAnInvalidContactId() {

        response = contactApi.getContactByInvalidContactId();
    }

    @Then("the status code should be {int}")
    public void statusCodeShouldBe(int code) {

        assertThat(response.getStatusCode()).isEqualTo(code);
    }

    @Then("the message should be {string}")
    public void messageShouldBe(String message) {

        assertThat(response.xmlPath().getString("error.message")).isEqualTo(message);
    }

    @Then("new contact is available with")
    public void newContactIsAvailable(List<Map<String,String>> data) {
        contact = response.getBody().as(Contact.class);
        assertThat(contact).isNotNull();
        assertThat(contact.getContactNumber()).isEqualTo(data.get(0).get("contactNumber").replace("-",""));
        assertThat(contact.getFirstName()).isEqualTo(data.get(0).get("firstName"));
        assertThat(contact.getLastName()).isEqualTo(data.get(0).get("lastName"));
        assertThat(contact.getTitle()).isEqualTo(data.get(0).get("title"));
    }

    @Then("contact is available with contact number {string}")
    public void newContactIsAvailableWithContactNumber(String contactNumber) {
        contact = response.getBody().as(Contact.class);
        assertThat(contact).isNotNull();
        assertThat(contact.getContactNumber()).isEqualTo(contactNumber);
    }

    @Then("the first contact is available")
    public void theResponseBodyIsFirstContactInList() {
        String contactId=contactApi.getAllContacts().xmlPath().getString("contacts.contact[0].contactId");
        String firstName=contactApi.getAllContacts().xmlPath().getString("contacts.contact[0].firstName");
        String lastName=contactApi.getAllContacts().xmlPath().getString("contacts.contact[0].lastName");
        String contactNumber=contactApi.getAllContacts().xmlPath().getString("contacts.contact[0].contactNumber");
        String title=contactApi.getAllContacts().xmlPath().getString("contacts.contact[0].title");
        contact = response.getBody().as(Contact.class);
        assertThat(contact.getContactId()).isEqualTo(contactId);
        assertThat(contact.getContactNumber()).isEqualTo(contactNumber);
        assertThat(contact.getFirstName()).isEqualTo(firstName);
        assertThat(contact.getLastName()).isEqualTo(lastName);
        assertThat(contact.getTitle()).isEqualTo(title);
    }
}


