package zoomconnectapp.api;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import model.Contact;
import zoomconnectapp.specifications.ReusableSpecifications;

import java.util.*;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ContactApi {

    @Step("Get all contacts with valid token")
    public Response getAllContacts() {
        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .get("/all")
                .then()
                .extract()
                .response();
    }

    @Step("Get all contacts with invalid token")
    public Response getAllContactsWithInvalidToken() {
        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithInvalidToken())
                .when()
                .get("/all")
                .then()
                .extract()
                .response();
    }

    @Step("Create a contact")
    public Response createContact(List<Map<String, String>> contacts) {

        Contact contact = new Contact();
        contact.setContactNumber(contacts.get(0).get("contactNumber"));
        contact.setFirstName(contacts.get(0).get("firstName"));
        contact.setLastName(contacts.get(0).get("lastName"));
        contact.setTitle(contacts.get(0).get("title"));

        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .body(contact)
                .post("/create")
                .then()
                .extract()
                .response();

    }

    @Step("Delete a contact")
    public Response deleteContact() {
        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .delete("/" + getContactId());
    }

    @Step("Delete a invalid contact")
    public Response deleteInvalidContact() {
        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .delete("/" + getContactId() + "invalid");
    }

    @Step("Get contact by contactId")
    public Response getContactByContactId() {

        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .get("/" + getContactId())
                .then()
                .extract()
                .response();
    }

    @Step("Get contact by invalid contactId")
    public Response getContactByInvalidContactId() {

        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .get("/" + getContactId() + "invalid");
    }

    @Step("Update contact")
    public Response updateContact(String contactNumber) {
        Contact contact = getContactByContactId().getBody().as(Contact.class);

        Contact updatedContact = new Contact();
        updatedContact.setContactNumber(contactNumber);
        updatedContact.setFirstName(contact.getFirstName());
        updatedContact.setLastName(contact.getLastName());
        updatedContact.setTitle(contact.getTitle());
        String contactId = contact.getContactId();

        return SerenityRest.rest()
                .given()
                .spec(ReusableSpecifications.setRequestSpecWithValidToken())
                .when()
                .body(updatedContact)
                .post("/" + contactId)
                .then()
                .extract()
                .response();
    }

    /**
     * method for getting contactID if available, if not, new contact is created
     *
     * @return value of the first contactId in the list
     */
    public String getContactId() {
        String contactId;
        Map<String, String> firstContact = new HashMap<>();
        firstContact.put("firstName", "Jane");
        firstContact.put("lastName", "Doe");
        firstContact.put("contactNumber", "+17159414024");
        firstContact.put("title", "Hello");
        List<Map<String, String>> data = Collections.singletonList(firstContact);
        if (getAllContacts().xmlPath().getString("contacts.contact.contactId").isEmpty()) {
            Contact contact = createContact(data).getBody().as(Contact.class);
            assertThat(contact).isNotNull();
            contactId = contact.getContactId();
        } else {
            contactId = getAllContacts().xmlPath().getString("contacts.contact[0].contactId");
        }
        return contactId;
    }

}




