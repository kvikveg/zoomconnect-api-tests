package zoomconnectapp.specifications;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class ReusableSpecifications {

    public static RequestSpecBuilder rspec;
    public static RequestSpecification requestSpecification;
    private  static final String EMAIL="draganakuzminac@gmail.com";
    private static final String TOKEN="17448ac3-c866-49af-9d0e-b3fd75303778";
    private static final EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
    private static final String BASE_URI =  EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("baseURI");
    private static final String BASE_PATH =  EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("basePath");

    public static RequestSpecification setRequestSpecWithValidToken(){

        rspec = new RequestSpecBuilder();
        rspec.setBaseUri(BASE_URI);
        rspec.setBasePath(BASE_PATH);
        rspec.addHeader("email", EMAIL);
        rspec.addHeader("token", TOKEN);
        rspec.setContentType(ContentType.JSON);
        requestSpecification = rspec.build();
        return requestSpecification;

    }

    public static RequestSpecification setRequestSpecWithInvalidToken(){

        rspec = new RequestSpecBuilder();
        rspec.setBaseUri(BASE_URI);
        rspec.setBasePath(BASE_PATH);
        rspec.addHeader("email", EMAIL);
        rspec.addHeader("token", "");
        rspec.setContentType(ContentType.JSON);
        requestSpecification = rspec.build();
        return requestSpecification;

    }

}
