package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact {

    private String contactId ;
    private String contactNumber;
    private String firstName;
    private String lastName;
    private String title;

}
